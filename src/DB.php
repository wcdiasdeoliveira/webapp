<?php
	namespace Application;

	class DB extends \SQLite3
	{
		private $name;

		private $path;

		private $resource;

		public function openResource()
		{
			$this->name = "fotos";
			$this->path = "../data/";
			$resource = $this->path.$this->name.".db";
			parent::open($resource);
		}

		public function setUp()
		{
			$this->openResource();
			$sql ="CREATE TABLE IF NOT EXISTS fotos
			(ID INTEGER PRIMARY KEY AUTOINCREMENT,
			URL TEXT NOT NULL,
			TITLE TEXT NOT NULL
			);";
			$result = $this->exec($sql);
			if(!$result){
				echo $this->lastErrorMsg();
				exit;
			}
			$this->close();
		}

		public function __construct()
		{
			chdir(dirname(__FILE__));
			$this->setUp();
		}

		public function save($file,$title)
		{
			$this->openResource();
			$result = false;
			$sql ="INSERT INTO fotos(URL,TITLE) VALUES('$file','$title')";
			$result = $this->exec($sql);
			if(!$result){
				echo $this->lastErrorMsg();
			}
			$this->close();
			return $result;
		}

		public function findById($id)
		{
			$this->openResource();
			$result = [];
			$sql ="SELECT * FROM fotos WHERE ID = $id";
			$rows = $this->query($sql);
			while($row = $rows->fetchArray(SQLITE3_ASSOC) ){
				$result[] = $row;
			}
			$this->close();
			return $result[0];
		}

		public function fetchAll()
		{
			$this->openResource();
			$result = [];
			$sql ="SELECT * FROM fotos";
			$rows = $this->query($sql);
			while($row = $rows->fetchArray(SQLITE3_ASSOC) ){
				$result[] = $row;
			}
			$this->close();
			return $result;
		}

		public function remove($id)
		{
			$this->openResource();
			$result = false;
			$sql ="DELETE FROM fotos WHERE ID = $id";
			$result = $this->exec($sql);
			if(!$result){
				echo $this->lastErrorMsg();
			}
			$this->close();
			return $result;
		}
	}
