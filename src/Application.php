<?php
	namespace Application;

	class Application
	{
		private $app_title;

		private $links = array();

		public function setAppTitle(string $title)
		{
			$this->app_title = $title;
		}

		public function getAppTitle(): string
		{
			return $this->app_title;
		}

		protected function setLinks(array $links)
		{
			$this->links = $links;
		}

		protected function getLinks()
		{
			return $this->links;
		}

		public function __construct()
		{
			$this->setAppTitle(".: WebApp :.");
			$this->setLinks(array(
			[ 'url' => 'index.php', 'title' => 'Home' ],
			[ 'url' => 'src/router.php?option=1', 'title' => 'Cadastrar foto' ],
			[ 'url' => 'src/router.php?option=2', 'title' => 'Ver fotos' ]
			)
			);
		}

		public function buildTopBar()
		{
			$topBar = "
			<!-- Navigation -->
			<nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>
			<div class='container'>
			<!-- Brand and toggle get grouped for better mobile display -->
				<div class='navbar-header'>
			<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>
			<span class='sr-only'>Toggle navigation</span>
			<span class='icon-bar'></span>
			<span class='icon-bar'></span>
			<span class='icon-bar'></span>
			</button>
			<a class='navbar-brand' href='README.html' target='_blank'> README </a>

			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
				<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
			<ul class='nav navbar-nav'>
			<li>
			<a href='index.php'>Home</a>
			</li>
			<li>
			<a href='src/router.php?option=1'>Cadastrar foto</a>
			</li>
			<li>
			<a href='src/router.php?option=2'>Ver fotos</a>
			</li>
			</ul>
			</div>
			<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
			</nav>";
			return $topBar;
		}

		public function buildContent()
		{
			$topBar = $this->buildTopBar();
			$body = "";
			$links = "";
			foreach ($this->getLinks() as $link) {
				$url = $link['url'];
				$title = $link['title'];
				$links .= "<a href='$url'>$title</a> </br>";
			}
			$body .= $links;
			$content = array(
			'title' => $this->getAppTitle(),
			'topBar' => $topBar,
			'body' => $body
			);
			return $content;
		}
	}
