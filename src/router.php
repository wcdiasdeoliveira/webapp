<?php
    require_once "../vendor/autoload.php";

    use Application\FotoRepository;

    $option = $_GET['option'] ?? 6;

    switch ($option) {
    	case 1: // Cadastrar foto
    	case 2: // Listar foto
    	case 3: // Result upload foto
    	case 4: // Result upload foto
    	$uri = "../fotos.php?option=$option";
    	break;
    	case 5: // Remover foto
    	$fotoRepository = new FotoRepository();
    	$id = $_GET['id'];
    	$resource = $fotoRepository->remove($id) ? 5 : 6;
    	$uri = "../fotos.php?option=$resource";
    	break;
    	// Recebendo arquivo
    	case 6:
    	$fotoRepository = new FotoRepository();
    	$resource = $fotoRepository->save() ? 3 : 4;
    	$uri = "../fotos.php?option=$resource";
    	break;

    	default:
    	$uri = "../index.php";
    	break;
    }

    header("location:$uri");
