<?php
	use Application\Application;
	use Application\FotoMapper;

	namespace Application;

	class FotoRepository extends Application
	{
		private $photos = array();

		private $mode;

		private $mapper;

		private function setPhotos(array $photos)
		{
			$this->photos = $photos;
		}

		private function getPhotos()
		{
			return $this->photos;
		}

		private function setMode(int $mode)
		{
			$this->mode = $mode;
		}

		private function getMode()
		{
			return $this->mode;
		}

		private function getMapper()
		{
			if($this->mapper == NULL){
				$this->mapper = new FotoMapper();
			}
			return $this->mapper;
		}


		public function __construct(int $mode = 0)
		{
			parent::setAppTitle(".: WebApp - Fotos :.");
			$this->setMode($mode);
			if($this->getMode() == 2){
				$this->setPhotos($this->getMapper()->fetchAll());
			}
		}

		public function buildContent()
		{
			$content = parent::buildContent();
			if($this->getMode() == 1){
				$content['body'] = "
				<form enctype='multipart/form-data' action='src/router.php' method='POST'>
				Enviar esse arquivo: <input name='file' type='file' id='file' />
				<input type='submit' value='Enviar arquivo' />
				</form>";
			}else if($this->getMode() == 2){
				$photos = "<h1 class='page-header'>Thumbnail Gallery</h1>";
				foreach ($this->getPhotos() as $photo) {
					$url = $photo['URL'];
					$title = $photo['TITLE'];
					$id = $photo['ID'] ?? 0;
					$resouce = "option=5&id=$id";
					$photos .= "<div class='col-lg-3 col-md-4 col-xs-6 thumb'>
					<a class='thumbnail' href='#'>
					<img class='img-responsive' src='$url' alt='$title'>
					</a>

					<button class='btn btn-danger btn-small'>
					<a href='src/router.php?$resouce'>Excluir</a>
					</button>
					</div>
					";
				}
				$content['body'] = $photos;
			}else{
				if($this->getMode() == 3 || $this->getMode() == 4){
					$content['body'] = $this->getMode() == 3 ? "<h1> Foto inserida com sucesso. </h1>" : "<h1> Falha ao salvar arquivo. Extensão não permitida. </h1>";
				}else if($this->getMode() == 5 || $this->getMode() == 6){
					$content['body'] = $this->getMode() == 5 ? "<h1> Foto removida com sucesso. </h1>" : "<h1> Falha ao remover arquivo. </h1>";
				}
			}
			return $content;
		}

		public function save()
		{
			return $this->getMapper()->save();
		}

		public function findById($id)
		{

		}

		public function fetchAll()
		{
			return array(
			[ 'url' => 'assets/img/todas-fotos.jpg', 'title' => '' ],
			[ 'url' => 'assets/img/todas-fotos.jpg', 'title' => '' ],[ 'url' => 'assets/img/todas-fotos.jpg', 'title' => '' ],[ 'url' => 'assets/img/todas-fotos.jpg', 'title' => '' ],
			[ 'url' => 'assets/img/todas-fotos.jpg', 'title' => '' ]
			);
		}

		public function remove($id)
		{
			return $this->getMapper()->remove($id);
		}
	}
