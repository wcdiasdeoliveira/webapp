FROM centos:centos7

RUN yum clean all
RUN yum update -y
RUN yum install -y --setopt=tsflags=nodocs \
    https://rpms.remirepo.net/enterprise/remi-release-7.rpm
RUN yum install -y --setopt=tsflags=nodocs --enablerepo=remi,remi-php70 \
    php php-gd php-xml php-mbstring php-mcrypt php-mysqlnd php-pecl-apcu \
    httpd supervisor sqlite \

COPY assets /var/www/html/webapp/assets
COPY data /var/www/html/webapp/data
COPY src /var/www/html/webapp/src
COPY tests /var/www/html/webapp/tests
COPY vendor /var/www/html/webapp/vendor
COPY composer.json /var/www/html/webapp
COPY fotos.php /var/www/html/webapp
COPY index.php /var/www/html/webapp
COPY composer.json /var/www/html/webapp
COPY supervisord.conf /etc/supervisord.conf
COPY setup.sh /etc/setup.sh
COPY README.html /var/www/html/webapp

CMD ["/usr/bin/supervisord"]

EXPOSE 80
