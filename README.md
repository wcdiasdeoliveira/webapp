## Synopsis

This is just a demo app, only for comprove some skills. It's a simple web application built  with pure PHP(7).

I've tried to keep the whole thing simple so I've avoided coments on code.

## Installation

Clone the repository:

git clone https://wcdiasdeoliveira@bitbucket.org/wcdiasdeoliveira/webapp.git

Run the app using the following commands inside the webapp directory:

php -S <HOST>:<PORT>

Or:

docker build .b -t <IMAGE_NAME>
docker run --detach <IMAGE_NAME>

## Tests

Enter:

cd tests

Then:

phpunit

## Bugs

On the current version, if a file with the same name exists in the assets directory, it will be replaced
and will occur two rows with the same value in the database.

## Contributors

Wesley C. Dias de Oliveira

## License

Free for use and extend.
