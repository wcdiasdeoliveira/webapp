<?php
    use Application\Application;

    class ApplicationTest extends \PHPUNIT_Framework_TestCase
    {
    	private $application;

    	protected function setUp()
    	{
    		$this->application = new Application();
    	}

    	public function testObjectInterface()
    	{
    		$this->assertFalse(empty($this->application->getAppTitle()));
    		$this->assertFalse(empty($this->application->buildTopBar()));
    		$this->assertFalse(empty($this->application->buildContent()));
    	}
    }
