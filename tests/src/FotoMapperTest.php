<?php
    use Application\FotoMapper;

    class FotoMapperTest extends \PHPUNIT_Framework_TestCase
    {
    	private $fotoMapper;

    	protected function setUp()
    	{
    		$this->fotoMapper = new FotoMapper();
    	}

    	public function testObjectInterface()
    	{
    		$crudMethods = [ 'save', 'findById', 'fetchAll', 'remove' ];
    		foreach ($crudMethods as $method ) {
    			$this->assertTrue(method_exists($this->fotoMapper, $method));
    		}
    	}
    }
