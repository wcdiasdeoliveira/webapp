<?php
    use Application\FotoRepository;

    class FotoRepositoryTest extends \PHPUNIT_Framework_TestCase
    {
    	private $fotoRepository;

    	protected function setUp()
    	{
    		$this->fotoRepository = new FotoRepository();
    	}

    	public function testObjectInterface()
    	{
    		$this->assertFalse(empty($this->fotoRepository->getAppTitle()));
    		$this->assertFalse(empty($this->fotoRepository->buildTopBar()));
    		$this->assertFalse(empty($this->fotoRepository->buildContent()));
    		$crudMethods = [ 'save', 'findById', 'fetchAll', 'remove' ];
    		foreach ($crudMethods as $method ) {
    			$this->assertTrue(method_exists($this->fotoRepository, $method));
    		}
    	}
    }
