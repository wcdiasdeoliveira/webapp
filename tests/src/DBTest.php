<?php
    use Application\DB;

    class DBTest extends \PHPUNIT_Framework_TestCase
    {
        private $db;

        protected function setUp()
        {
            $this->db = new DB();
        }

        public function testObjectInterface()
        {
          $this->assertInstanceOf('SQLite3', $this->db);
        }
    }
